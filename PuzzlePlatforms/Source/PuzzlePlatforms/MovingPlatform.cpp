// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlatform.h"

AMovingPlatform::AMovingPlatform() {

	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	SetMobility(EComponentMobility::Movable);
}

void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();
//	UE_LOG(LogTemp, Warning, TEXT("The Game Mode is not a VPGameMode."));
	if (HasAuthority())
	{
		SetReplicates(true);
		SetReplicateMovement(true);
	}

	GlobalStartLocation = GetActorLocation();
	GlobalTargetLocation = GetTransform().TransformPosition(TargetLocation);
}

void AMovingPlatform::AddActiveTrigger()
{
	ActiveTriggers++;
}

void AMovingPlatform::RemoveActiveTrigger()
{
	ActiveTriggers--;
}


void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (HasAuthority() && ActiveTriggers > 0)
	{
		auto Location = GetActorLocation();

		FVector Direction = (GlobalTargetLocation - GlobalStartLocation);
		
		float JournetLength = Direction.Size();

		float JournetTravelled = (Location - GlobalStartLocation).Size();
		auto Direct = Direction.GetSafeNormal();

		Location += Direction * DeltaTime * Speed;

		SetActorLocation(Location);

		if (JournetTravelled >= JournetLength)
		{
			auto swap = GlobalTargetLocation;
			GlobalTargetLocation = GlobalStartLocation;
			GlobalStartLocation = swap;
		}
	}

}
