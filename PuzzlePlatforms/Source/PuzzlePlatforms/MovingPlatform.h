// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingPlatform.generated.h"

/**
 * 
 */
UCLASS()
class PUZZLEPLATFORMS_API AMovingPlatform : public AStaticMeshActor
{
	GENERATED_BODY()
	

public:
	AMovingPlatform();

	UPROPERTY(EditAnyWhere)
	float Speed = 20;

	UPROPERTY(EditAnyWhere, Meta = (MakeEditWidget = true))
	FVector TargetLocation;

	virtual void Tick(float DeltaTime) override;
	virtual void BeginPlay() override;

	void AddActiveTrigger();
	void RemoveActiveTrigger();
private:
	FVector GlobalTargetLocation;
	FVector GlobalStartLocation;

	UPROPERTY(EditAnyWhere)
	int ActiveTriggers = 1;

};
